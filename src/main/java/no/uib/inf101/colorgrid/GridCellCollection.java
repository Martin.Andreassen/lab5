package no.uib.inf101.colorgrid;

import java.util.List;

/**
 * Represents a collection of grid cells of a generic type.
 * This interface allows for assembling a list of GridCell objects.
 *
 * @param <T> The type of elements in the grid cells.
 */
public interface GridCellCollection<T> {

    /**
     * Get a list containing the GridCell objects in this collection.
     *
     * @return a list of all GridCell objects in this collection.
     */
    List<GridCell<T>> getCells();
}
