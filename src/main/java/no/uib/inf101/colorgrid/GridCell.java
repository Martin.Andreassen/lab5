package no.uib.inf101.colorgrid;

import java.awt.Color;

/**
 * Represents a grid cell with a position and an element of generic type T.
 *
 * @param cellPosition the position of the cell in the grid
 * @param elem         the element of generic type T stored in this cell
 */
public record GridCell<T>(CellPosition pos, T elem) { }
