package no.uib.inf101.colorgrid;

// Husk å importere nødvendige klasser
import java.util.ArrayList;
import java.util.List;

public class Grid<T> implements IGrid<T> {
    private int numRows;
    private int numCols;
    private T[][] grid; // Den generiske arrayen for å lagre elementene.

    @SuppressWarnings("unchecked")
    public Grid(int numRows, int numCols) {
        this.numRows = numRows;
        this.numCols = numCols;
        this.grid = (T[][]) new Object[numRows][numCols];
    }

    @Override
    public int rows() {
        return numRows;
    }

    @Override
    public int cols() {
        return numCols;
    }

    @Override
    public T get(CellPosition pos) {
        if (pos.row() < 0 || pos.row() >= numRows || pos.col() < 0 || pos.col() >= numCols) {
            throw new IndexOutOfBoundsException("Position out of grid bounds");
        }
        return grid[pos.row()][pos.col()];
    }

    @Override
    public void set(CellPosition pos, T elem) {
        if (pos.row() < 0 || pos.row() >= numRows || pos.col() < 0 || pos.col() >= numCols) {
            throw new IndexOutOfBoundsException("Position out of grid bounds");
        }
        grid[pos.row()][pos.col()] = elem;
    }

    @Override
    public List<GridCell<T>> getCells() {
        List<GridCell<T>> list = new ArrayList<>();
        for (int row = 0; row < numRows; row++) {
            for (int col = 0; col < numCols; col++) {
                CellPosition position = new CellPosition(row, col);
                T elem = grid[row][col];
                list.add(new GridCell<>(position, elem));
            }
        }
        return list;
    }
}
