package no.uib.inf101.colorgrid;

import java.util.List;

public interface IGrid<T> extends GridDimension, GridCellCollection<T> {
    int rows();
    int cols();
    T get(CellPosition pos);
    void set(CellPosition pos, T elem);
    List<GridCell<T>> getCells();
}