package no.uib.inf101.gridview;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import javax.swing.JPanel;
import no.uib.inf101.colorgrid.IGrid; // Endre til det generiske grensesnittet
import no.uib.inf101.colorgrid.GridCell; // Importer den generiske GridCell klassen


public class TextGridView extends JPanel {

    private IGrid<String> grid; // Endre til den generiske typen for tekst
    private static final double OUTERMARGIN = 20;
    private static final double INNERMARGIN = 5;
    private static final Color MARGINCOLOR = Color.LIGHT_GRAY;

    public TextGridView(IGrid<String> textGrid) {
        this.grid = textGrid;
        this.setPreferredSize(new Dimension(1000, 300));
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        drawGrid(g2);
    }

    private void drawGrid(Graphics2D g2) {
        Rectangle2D box = new Rectangle2D.Double(OUTERMARGIN, OUTERMARGIN, this.getWidth() - OUTERMARGIN * 2,
                this.getHeight() - OUTERMARGIN * 2);

        g2.setColor(MARGINCOLOR);
        g2.fill(box);

        CellPositionToPixelConverter converter = new CellPositionToPixelConverter(box, grid, INNERMARGIN);
        drawCells(g2, grid, converter);
    }

	private static void drawCells(Graphics2D g2, IGrid<String> grid,
            CellPositionToPixelConverter converter) {
        Font font = new Font("SansSerif", Font.BOLD, 20); // Velg ønsket skrifttype og størrelse
        g2.setFont(font);
        FontMetrics metrics = g2.getFontMetrics(font);

        for (GridCell<String> cell : grid.getCells()) {
            Rectangle2D cellBox = converter.getBoundsForCell(cell.pos());
            String text = cell.elem();
            if (text == null || text.isEmpty()) continue;

            // Beregn x og y for å sentrere teksten i cellen
            int x = (int) cellBox.getX() + (int) ((cellBox.getWidth() - metrics.stringWidth(text)) / 2);
            int y = (int) cellBox.getY() + ((int) (cellBox.getHeight() - metrics.getHeight()) / 2) + metrics.getAscent();

            g2.setColor(Color.BLACK); // Sett skriftfarge
            g2.drawString(text, x, y);
        }
    }
}