package no.uib.inf101.datastructure;

import static org.junit.jupiter.api.Assertions.assertEquals;

import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.GridCell;
import org.junit.jupiter.api.Test;

public class GridCellTest {

    @Test
    public void sanityTest() {
        GridCell<Integer> integerCell = new GridCell<>(new CellPosition(5, 2), 40);
        assertEquals(40, integerCell.elem(), "element is not equal");
        CellPosition expectedPosition = new CellPosition(5, 2);
        assertEquals(expectedPosition, integerCell.pos(), "position is not equal");
    }
}
